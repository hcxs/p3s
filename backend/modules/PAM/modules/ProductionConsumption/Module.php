<?php

namespace backend\modules\PAM\modules\ProductionConsumption;

/**
 * production-consumption module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\PAM\modules\ProductionConsumption\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
