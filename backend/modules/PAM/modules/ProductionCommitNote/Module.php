<?php

namespace backend\modules\PAM\modules\ProductionCommitNote;

/**
 * production-commit-note module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\PAM\modules\ProductionCommitNote\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
