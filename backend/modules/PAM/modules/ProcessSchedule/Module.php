<?php

namespace backend\modules\PAM\modules\ProcessSchedule;

/**
 * process-schedule module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\PAM\modules\ProcessSchedule\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
