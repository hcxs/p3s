<?php

namespace backend\modules\Database\modules\Material;

/**
 * material module definition class
 */
class Module extends \kartik\tree\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\Database\modules\Material\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
