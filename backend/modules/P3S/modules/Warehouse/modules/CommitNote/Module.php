<?php

namespace backend\modules\P3S\modules\Warehouse\modules\CommitNote;

/**
 * commit-note module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\P3S\modules\Warehouse\modules\CommitNote\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
