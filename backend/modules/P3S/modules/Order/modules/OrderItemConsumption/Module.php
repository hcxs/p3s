<?php

namespace backend\modules\P3S\modules\Order\modules\OrderItemConsumption;

/**
 * order-item-consumption module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\P3S\modules\Order\modules\OrderItemConsumption\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
