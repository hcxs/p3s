<?php

namespace backend\modules\P3S\modules\Processing\modules\DeliveryNote;

/**
 * delivery-note module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\P3S\modules\Processing\modules\DeliveryNote\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
