<?php

namespace backend\modules\Statics\modules\WarehouseCommitNote;

/**
 * warehouse-commit-note module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\Statics\modules\WarehouseCommitNote\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
