<?php

namespace backend\modules\Statics\modules\DeliveryItems;

/**
 * delivery-items module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\Statics\modules\DeliveryItems\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
